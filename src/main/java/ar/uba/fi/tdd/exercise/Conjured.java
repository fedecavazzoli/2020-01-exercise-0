package ar.uba.fi.tdd.exercise;

public class Conjured extends Food {
    public Conjured(Item _item){
        this.item = _item;
    }

    public void updateQuality(){
        if(this.item.quality > 2){
            this.item.quality -= 2;
        }
        if(this.item.quality == 1){
            this.item.quality = 0;
        }
        if(this.item.sellIn > 0){
            this.item.sellIn -= 1;
        }
    }
}
