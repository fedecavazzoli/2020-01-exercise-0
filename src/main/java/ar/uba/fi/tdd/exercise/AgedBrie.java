package ar.uba.fi.tdd.exercise;

public class AgedBrie extends Food{
    public AgedBrie(Item _item){
        this.item = _item;
    }

    public void updateQuality(){
        if(this.item.quality < 50){
            this.item.quality += 1;
        }
        if(this.item.sellIn > 0){
            this.item.sellIn -= 1;
        }
    }
}
