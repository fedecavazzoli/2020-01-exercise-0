package ar.uba.fi.tdd.exercise;

public abstract class Food {

    public Item item;

    public String getName() {
        return item.Name;
    }

    public int getQuality() {
        return item.quality;
    }

    public int getSellIn() {
        return item.sellIn;
    }
}
