package ar.uba.fi.tdd.exercise;

public class BackstagePasses extends Food{
    public BackstagePasses(Item _item){
        this.item = _item;
    }

    public void updateQuality(){
        if(this.item.sellIn == 0 ){
            this.item.quality = 0;
        }
        if(this.item.sellIn < 5) {
            setQuality(3);
        }
        if(this.item.sellIn <= 10) {
            setQuality(2);

        }
        if(this.item.sellIn > 10 ){
            setQuality(1);
        }
        if(this.item.sellIn > 0){
            this.item.sellIn -= 1;
        }
    }

    private void setQuality(int _quality){
        if(this.item.quality + _quality < 50){
            this.item.quality += _quality;
            return;
        }
        this.item.quality = 50;
        return;
    }
}
