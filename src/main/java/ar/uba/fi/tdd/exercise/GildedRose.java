
package ar.uba.fi.tdd.exercise;

class GildedRose {
  Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            if (items[i].Name.equals("Aged Brie")) {
                AgedBrie item = new AgedBrie(items[i]);
                item.updateQuality();
                updateItem(items[i], item.getQuality(), item.getSellIn());
            }
            if(items[i].Name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                BackstagePasses item = new BackstagePasses(items[i]);
                item.updateQuality();
                updateItem(items[i], item.getQuality(), item.getSellIn());
            }
            if(items[i].Name.equals("Sulfuras, Hand of Ragnaros")) {
                Sulfuras item = new Sulfuras(items[i]);
                item.updateQuality();
                updateItem(items[i], item.getQuality(), item.getSellIn());
            }
            if(items[i].Name.equals("Conjured")){
                Conjured item = new Conjured(items[i]);
                item.updateQuality();
                updateItem(items[i], item.getQuality(), item.getSellIn());
            }
        }
    }

    private void updateItem(Item item, int _quality, int _sellIn){
        item.quality = _quality;
        item.sellIn = _sellIn;
        return;
    }
}
