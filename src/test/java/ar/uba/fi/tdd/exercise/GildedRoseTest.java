package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void agedBrie() {
			Item[] items = new Item[] { new Item("Aged Brie", 10, 15) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(app.items[0].Name).isEqualTo("Aged Brie");
			assertThat(app.items[0].sellIn).isEqualTo(9);
			assertThat(app.items[0].quality).isEqualTo(16);
	}

	@Test
	public void agedBrieQualityLessThanFifty(){
		Item[] items = new Item[] { new Item("Aged Brie", 1, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].sellIn).isEqualTo(0);
		assertThat(app.items[0].quality).isEqualTo(50);
	}

	@Test
	public void sulfuras(){
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 15) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].Name).isEqualTo("Sulfuras, Hand of Ragnaros");
		assertThat(app.items[0].sellIn).isEqualTo(10);
		assertThat(app.items[0].quality).isEqualTo(15);
	}

	@Test
	public void sulfurasQualityGraterThanFifty(){
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 1, 55) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].sellIn).isEqualTo(1);
		assertThat(app.items[0].quality).isEqualTo(55);
	}

	@Test
	public void backstagePasses(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 15) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].Name).isEqualTo("Backstage passes to a TAFKAL80ETC concert");
		assertThat(app.items[0].sellIn).isEqualTo(9);
		assertThat(app.items[0].quality).isEqualTo(17);
	}

	@Test
	public void backstagePassesQualityGraterThanZero(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 20, 4) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(app.items[0].sellIn).isEqualTo(19);
		assertThat(app.items[0].quality).isEqualTo(5);
	}

	@Test
	public void conjured(){
			Item[] items = new Item[] { new Item("Conjured", 20, 4) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(app.items[0].Name).isEqualTo("Conjured");
			assertThat(app.items[0].sellIn).isEqualTo(19);
			assertThat(app.items[0].quality).isEqualTo(2);
	}
}
